// Ashley Rodrigues UNSW
// z5015076@cse.unsw.edu.au


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Console;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;


public class cdht {
	private String[] args;
	private static int myID;
	private static ArrayList<Integer> predPeers;
	private static ArrayList<Integer> succPeers;

	
	public cdht (String[] args) {
		this.args = args;
		myID =Integer.parseInt(args[0]) ;
		predPeers = new ArrayList<Integer>();
		succPeers = new ArrayList<Integer>();
		succPeers.add(Integer.parseInt(args[1]));
		succPeers.add(Integer.parseInt(args[2]));
		predPeers.add(0);
		predPeers.add(0);

	}	
	
	// Creates new cdht object and starts the cdht
	public static void main(String[] args) {
		cdht clientServer = new cdht(args);
		clientServer.startCDHT();
		

	}

	// Starts the CDHT by starting the TCPServer and UDPServer
	// which listens for incoming connections
	private void startCDHT () {
		//System.out.println("Starting cdht");
		TCPServer tcpS = new TCPServer(myID, predPeers, succPeers);
		UDPServer udpS = new UDPServer(myID, predPeers, succPeers);
		Thread t = new Thread(tcpS);
		Thread t1 = new Thread(udpS);
		t.start();
		t1.start();	
	}
}

// The UDP server thread listens for incoming udp connections
// Additionally a response message is sent back over the udp 
// connection if the incoming message was a ping
class UDPServer implements Runnable {

	private int recievePort;
	private InetAddress localIP;
	private int myID;
	private DatagramSocket socket;
	private int status = 0;
	private ArrayList<Integer> predPeers;
	private ArrayList<Integer> succPeers;
	
	public UDPServer (int myID, ArrayList<Integer> predPeers, ArrayList<Integer> succPeers) {
		this.myID = myID;
		this.predPeers  = predPeers;
		this.succPeers = succPeers;

		recievePort = myID + 50000;

	}
	
	@Override
	public void run() {		
		try {
			socket =  new DatagramSocket(recievePort);
			localIP = socket.getInetAddress();
			status = 1;
		} catch (SocketException e1) {
			status = 0;
			System.out.println("Address is already in use");
			e1.printStackTrace();
		}
		UDPPinger peer = new UDPPinger(socket, succPeers, 0);
		UDPPinger sucPeer = new UDPPinger(socket, succPeers, 1);
		Thread t = new Thread(peer);
		Thread t1 = new Thread(sucPeer);
		t.start();
		t1.start();
		
		while (status == 1) {
			try {
				DatagramPacket request = new DatagramPacket(new byte[1024], 1024);
				socket.receive(request);
				byte[] buf = request.getData();
			    ByteArrayInputStream bais = new ByteArrayInputStream(buf);
			    InputStreamReader isr = new InputStreamReader(bais);
			    BufferedReader br = new BufferedReader(isr);	
			    String line = br.readLine();
			    
			    // If incoming is a ping message send a response message
			    if (line.startsWith("Ping", 0)) {			    
			    	System.out.println("A ping message was received from "
			    								+ (request.getPort()- 50000));		    	
					predPeers.set(Integer.parseInt(line.split(" ")[1].trim()), request.getPort()-50000);
			    	String message = "Resp";
				    byte[] sendData  = new byte[1024]; 
				    sendData = message.getBytes(); 
			        DatagramPacket sendPacket =  new DatagramPacket(sendData, sendData.length,
			        		request.getAddress(), request.getPort()); 			       
			        socket.send(sendPacket);
			        
			    // If the incoming message is a response message send to stdout    
			    } else if (line.startsWith("Resp", 0)) {
					System.out.println("A response message was received from "
							+ (request.getPort()- 50000)); 			    	
			    } 	    	
			} catch (IOException e) {					
				System.out.println("Triggered");
				System.out.println(e.getStackTrace());
				socket.close();
				status = 0;
				break;
			}
		}
	}	
}

// Purpose of this class is to operate independently and ping successor peers
// Using the udp connection of the socket this thread sends udp packets
// after a certain time period has passed
class UDPPinger implements Runnable {
	private DatagramSocket socket;
	private InetAddress localIP;
	private int targetPort;
	private ArrayList<Integer> succPeers;
	private int succPos;
	
	public UDPPinger (DatagramSocket socket, ArrayList<Integer> succPeers, int succPos) {
		this.socket = socket;
		this.targetPort = 50000 + targetPort;
		this.succPeers = succPeers;
		this.succPos = succPos;
	}
	// Starts pinging successor peers at a constant interval
	@Override
	public void run() {
		try {
			while (true) {
				localIP = InetAddress.getLocalHost();
				String message = "Ping " + succPos;
				byte[] buf = message.getBytes();
				DatagramPacket reply = new DatagramPacket(buf, buf.length, localIP, (50000+succPeers.get(succPos)));
				socket.send(reply);
				Thread.sleep(4000);
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

// Retrieves standard input from the terminal and handles accordingly
// Two cases: input is some 'request' or 'quit' command
class StdInReader implements Runnable {
	private int myID;
	private Socket clientSocket;
	private ArrayList<Integer> predPeers;
	private ArrayList<Integer> succPeers;
	
	public StdInReader (int myID, ArrayList<Integer> predPeers, ArrayList<Integer> succPeers) {
		this.myID = myID;
		this.predPeers = predPeers;
		this.succPeers = succPeers;
	}
	
	// Listens for input from standard input and calls 
	// appropriate functions to handle both cases
	// case 1: request--> request for a file
	// case 2: quit --> command to quit cdht
	// 		and remove this peer from the network
	@Override
	public void run() {
		while (true) {
			Console console = System.console();
			String input = console.readLine();
			if (input.split(" ")[0].equals("request")) {
				askPeer(input);
			} else if (input.split(" ")[0].equals("quit")) {
				System.out.println("Peer " + myID + " will depart from the network.");
				departMsg();
			}
		}
	}

	// Initiates a connection with predecessor peers and informs them that this peer
	// is leaving the network, providing the successors of this peer along
	// in  the message 
	private void departMsg()  {		
		boolean connectionStatus = false;
		// Tries to connect to first peer and informs predecessor peer
		// of the current peer leaving the network
		try {
			clientSocket = new Socket("localhost", 50000 + predPeers.get(0));
			connectionStatus = true;
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
		
		if (connectionStatus) {
			try {
				String sentence = "depart " + myID + " " + succPeers.get(0) +  " " + succPeers.get(1);
				DataOutputStream outToServer; 
				outToServer = new DataOutputStream(clientSocket.getOutputStream());
			 	outToServer.writeBytes(sentence + '\n');
		  		clientSocket.close();
		  		connectionStatus = false;
			} catch (IOException e) {
				System.out.println("IO Exception");
				e.printStackTrace();
			}
		}
		// Tries to connect to second peer and informs predecessor peer
		// of the current peer leaving the network		
		try {
			clientSocket = new Socket("localhost", 50000 + predPeers.get(1));
			connectionStatus = true;
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
		if (connectionStatus) {
			try {
				String sentence = "depart " + myID + " " + succPeers.get(0) +  " " + succPeers.get(1);
				DataOutputStream outToServer; 
				outToServer = new DataOutputStream(clientSocket.getOutputStream());
			 	outToServer.writeBytes(sentence + '\n');
		  		clientSocket.close();
		  		connectionStatus = false;
			} catch (IOException e) {
				System.out.println("IO Exception");
				e.printStackTrace();
			}
		}
		System.exit(0);
	}
	
	// Initiates a tcp connection with its successor peer
	// and asks whether it contains the 'file' being requested
	private void askPeer(String fileName)  {		
		boolean connectionStatus = false;
		try {
			clientSocket = new Socket("localhost", (50000 + succPeers.get(0)));
			connectionStatus = true;
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}
		if (connectionStatus) {
			try {
				String splitString[] = fileName.split(" ");
				String sentence = "prequest " + splitString[1] + " " + myID + " " + myID;
				DataOutputStream outToServer;
				outToServer = new DataOutputStream(clientSocket.getOutputStream());
			 	outToServer.writeBytes(sentence + '\n');
		  		clientSocket.close();
		  		System.out.println("File request for " + splitString[1] + " has been sent to "
						+ "my successor");
			} catch (IOException e) {
				System.out.println("IO Exception");
				e.printStackTrace();
			}
		}
	}
	
}

// Initiates TCP server which receives TCP connections
// Received TCP connections are handled by the clienthandler class
class TCPServer implements Runnable {
	private int myID;
	private ServerSocket tcpSocket;
	private boolean tcpServerOn;
	private ArrayList<Integer> predPeers;
	private ArrayList<Integer> succPeers;
	
	
	public TCPServer (int myID, ArrayList<Integer> predPeers, ArrayList<Integer> succPeers) {
		this.myID = myID;
		this.predPeers = predPeers;
		this.succPeers = succPeers;
		this.tcpServerOn = false;
		
	}

	// Starts TCP server and standard input reader
	// Standard input reader will only only run if 
	// tcp server is able to turn on
	@Override
	public void run() {
		
		try {
			tcpSocket = new ServerSocket(50000  + myID);
			tcpServerOn = true;
		} catch (IOException e) {		
			System.out.println("IO Exception");
			e.printStackTrace();		}
		if (tcpServerOn) {
			StdInReader sr = new StdInReader(myID, predPeers, succPeers);
			Thread t = new Thread(sr);
			t.start();
		
			while (true) {
				try {
					Socket newTCPLink = tcpSocket.accept();
					Thread t1 = new Thread( new TCPClientHandler(newTCPLink, myID, succPeers));
					t1.start();
				} catch (Exception e) {
					System.out.println("IO Exception");
					e.printStackTrace();				}
			}
		}
	}
	
}


// Hashing class
// Given a file name a hash will be retrieved according to
// the hash being used by that machine
class FileHash {
	private String fileName;
	
	public FileHash (String fileName) {
		this.fileName = fileName;
	}
	
	public int getFileHash () {
		char splitDigits[] = fileName.toCharArray();
		int hashKey = 0;
		int i = (int) Math.pow(10, (splitDigits.length- 1));
		int z = 0; 

		
		for (char digit : splitDigits) {
			z = Character.getNumericValue(digit);
			hashKey += z * i;
			i = i/10;
		}
		return hashKey % 256;
	}
}

// Used to handle TCP connections receiveds by the server independently
class TCPClientHandler implements Runnable {
	private Socket actvConnec;
	private int predPeerID;
	private int hashCode;
	private int myID;
	private ArrayList<Integer> succPeers;
	
	TCPClientHandler (Socket actvConnec, int myID, ArrayList<Integer> succPeers) {
		this.actvConnec = actvConnec;
		this.myID = myID;
		this.succPeers = succPeers;
	}
	
	// Handles incoming packets along the tcp connection
	// case 1: string begins with ffound, which means that the file was found
	// case 2: the source peer is intending on departing from the network
	// default: request for a file from a peer
	@Override
	public void run() {
		BufferedReader inFromServer;
		try {
			inFromServer = new BufferedReader(new InputStreamReader(actvConnec.getInputStream()));
			String sentenceFromServer = inFromServer.readLine();
			String splitString[] = sentenceFromServer.split(" ");
			
			if (splitString[0].equals("ffound")) {
				System.out.println("Response was recieved from peer " + splitString[2] 
						+ ", which has the file " + splitString[1]);
			} else if (splitString[0].equals("depart")) {
				System.out.println("Peer " + splitString[1] + " will depart from the network.");
				if (Integer.parseInt(splitString[1]) == succPeers.get(0)) {
					succPeers.set(0, Integer.parseInt(splitString[2]));
					succPeers.set(1, Integer.parseInt(splitString[3]));
				} else if  (Integer.parseInt(splitString[1]) == succPeers.get(1)) {
					succPeers.set(1, Integer.parseInt(splitString[2]));
				}
				System.out.println("My first successor is now peer " + succPeers.get(0) + ".");
				System.out.println("My first successor is now peer " + succPeers.get(1) + ".");
			}else {
				predPeerID = Integer.parseInt(splitString[3]);
				System.out.println(predPeerID);
				FileHash fh = new FileHash(splitString[1]);
				hashCode = fh.getFileHash();
				if (isCloserToMe()) {
					System.out.println("File " + splitString[1] + " is here.");
					System.out.println("A response message, destined for peer " +
					splitString[2] + " , has been sent.");
					tellFileFound(Integer.parseInt(splitString[2]), splitString[1]);
				} else {
					askPeer(sentenceFromServer, splitString);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
				
	}
	
	// Checks whether the files hash value is closer to the current peer
	// e.g whether peer contains file
	// Returns true of so else return false if the file is closer
	// to the successor
	// Checks 3 base cases, last element, first element and element in between
	// the first and last element of the dht
	private boolean isCloserToMe () {
		int myIDDistance = Math.abs(hashCode - myID);
		int peerIDDistance = Math.abs(hashCode - succPeers.get(0));
		//((myID < succPeers.get(0)) && (myID < predPeerID))
		if (myID == hashCode) return true;

		System.out.println(myID + " " + succPeers.get(0) + " " + predPeerID + " " );
		if ((myID >  succPeers.get(0)) && (myID > predPeerID)) {	
			if ((hashCode < myID) && (hashCode > predPeerID)){ 
				System.out.println("yep");
				return true;
			}
		} else if ((myID < predPeerID) && (myID < succPeers.get(0))) {
			if ((hashCode > predPeerID)
					&& (myID < hashCode)) {
			return true;
			}
		} else  {
			if ((myIDDistance <= peerIDDistance) && (hashCode > predPeerID)
														&& (myID > hashCode)) {
				return true;
			}
		}
		return false;
	}
	
	// If the file is found/ contained within the peer informs the appropriate peer who 
	// was searching for the file
	private void tellFileFound (int destPort, String fileName) {
		boolean connectionStatus = false;
		try {
			actvConnec = new Socket("localhost", 50000 + destPort);
			connectionStatus = true;
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();		}
		if (connectionStatus) {
			try {
				String sentenceFromServer = "ffound " + fileName + " " + myID;
				DataOutputStream outToServer;
				outToServer = new DataOutputStream(actvConnec.getOutputStream());
			 	outToServer.writeBytes(sentenceFromServer + '\n');
		  		actvConnec.close();
		  		connectionStatus = false;
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	// Initiates a tcp connection with immediate successor peer immediate 
	// and asks the successor peer whether it contains the file
	// which is currently requested
	private void askPeer(String sentenceFromServer, String splitString[])  {
		System.out.println("File request for " + sentenceFromServer.split(" ")[1]
				+ " has been sent to " + "my successor");
		boolean connectionStatus = false;
		try {
			actvConnec = new Socket("localhost", 50000 + succPeers.get(0));
			connectionStatus = true;
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();		}
		if (connectionStatus) {
			try {
				DataOutputStream outToServer;
				outToServer = new DataOutputStream(actvConnec.getOutputStream());
			 	outToServer.writeBytes(splitString[0] + " " + splitString[1] 
			 			+ " " + splitString[2] + " " + myID + '\n');
		  		actvConnec.close();
				connectionStatus = false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
		
}
